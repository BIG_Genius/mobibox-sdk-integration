package com.mobileartsme.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ma.paymentsdk.MA_BillingActivity;
import com.ma.paymentsdk.MA_Lookup;
import com.ma.paymentsdk.interfaces.MA_OnLookUp;
import com.ma.paymentsdk.objects.MA_Billing;
import com.ma.paymentsdk.utilities.Logcat;
import com.ma.paymentsdk.utilities.MA_Utility;

public class SplashScreenActivity extends AppCompatActivity implements MA_OnLookUp {

    public static final String tag = "SplashScreenActivity";
    private final int SDK_CODE = 11;
    MA_Lookup lookup;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_splashscreen);
            navigateNext();
        } catch (Exception e) {
            Logcat.e(tag, "home error " + e.toString());
        }
    }

    private void navigateNext() {
        Logcat.e(tag, "nav next called");
        if (MA_Utility.isOnline(SplashScreenActivity.this)) {

            if (MA_Billing.getBillingStatus(SplashScreenActivity.this)) {
                callLookup();
            } else {
                Intent intent = new Intent(SplashScreenActivity.this, MA_BillingActivity.class);
                startActivityForResult(intent, SDK_CODE);
            }
        } else {
            Toast.makeText(SplashScreenActivity.this,
                    R.string.no_internet_connexion, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void callLookup() {
        lookup = new MA_Lookup(SplashScreenActivity.this);
        lookup.LookUpSubscriber(SplashScreenActivity.this);
    }

    private void goToMainActivity() {

        Intent browserIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(browserIntent);
        finish();
    }



    public void lookupResult(int status, String Description, Boolean isFreeTrial, String subscriptionDate, String expiryDate) {

        Logcat.e(tag, "User status: " + status + " description: " + Description);

        if (MA_Billing.isAppNeedsUpdate(SplashScreenActivity.this)) {
            //Means app needs update but user did not update, then close app
            finish();
            return;
        }
        try {
            //Status values:
            //0 for active users - subscribed and billed users
            //1 for none active users - subscribed users but not billed due to low balance
            //2 for deleted users - unsubscribed users
            //3 user not found
            if (status == 2 || status == 3) {
                //unsubscribed or not found users
                GlobalApplication.isPremium = false;
                GlobalApplication.isActiveUser = false;
                //open billing activity again
                Intent intent = new Intent(SplashScreenActivity.this, MA_BillingActivity.class);
                startActivityForResult(intent, SDK_CODE);
            } else if (status == 1) {
                //subscribed users but not billed due to low balance
                GlobalApplication.isPremium = true;
                GlobalApplication.isActiveUser = false;
                goToMainActivity();
            } else {
                //active users - subscribed and billed users
                GlobalApplication.isActiveUser = true;
                GlobalApplication.isPremium = true;
                goToMainActivity();
            }
        } catch (Exception e) {
            //Exception occurred, manage the case as you wish
            GlobalApplication.isActiveUser = true;
            GlobalApplication.isPremium = true;
            goToMainActivity();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logcat.e(tag, "onActivityResult");

        if (requestCode == SDK_CODE) {

            if (MA_Billing.isAppNeedsUpdate(SplashScreenActivity.this)) {
                //Means app needs update but user did not update, then close app
                finish();
                return;
            }

            if (resultCode == RESULT_OK) {
                if (MA_Billing.getBillingStatus(SplashScreenActivity.this)) {
                    //User has successfully subscribed, and you can grant him access to the premium features/content
                    GlobalApplication.isActiveUser = true;
                    GlobalApplication.isPremium = true;

                } else {
                    GlobalApplication.isActiveUser = false;
                    GlobalApplication.isPremium = false;
                    //User did not subscribe, check if the app has free version or not

                }
                goToMainActivity();
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
                Logcat.e(tag, "Result: Payment Unsuccessful");
                GlobalApplication.isActiveUser = false;
                GlobalApplication.isPremium = false;
                goToMainActivity();
            }
        }  else if (requestCode == MA_Lookup.RC_READ) {
            Logcat.e(tag, "onActivityResult: requestCode == MA_Lookup.RC_READ");
            if (lookup != null) {
                lookup.onActivityResult(requestCode, resultCode, data);
            }
        }
    }





}