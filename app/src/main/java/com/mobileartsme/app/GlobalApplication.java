package com.mobileartsme.app;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.ma.paymentsdk.MobiboxConfig;

import java.util.Locale;


public class GlobalApplication extends MultiDexApplication {

    public static boolean isPremium = false;
    public static boolean isActiveUser = false;
    public static boolean isFreeTrial = false;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        String language = Locale.getDefault().toString();
        MobiboxConfig config = new MobiboxConfig(this, false, language, true);

    }

}