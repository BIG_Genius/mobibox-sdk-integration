package com.mobileartsme.app.objects;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.ma.paymentsdk.broadcast.MA_CampaignTrackingReceiver;

/**
 * Created by Rouba on 9/19/2016.
 */
public class InstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        // Google Analytics
        new CampaignTrackingReceiver().onReceive(context, intent);

        // Mobibox SDK
        new MA_CampaignTrackingReceiver().onReceive(context, intent);
    }
}
