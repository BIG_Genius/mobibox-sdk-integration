package com.mobileartsme.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.paymentsdk.MA_BillingActivity;
import com.ma.paymentsdk.interfaces.EventTrackerListener;
import com.ma.paymentsdk.objects.EventHandler;
import com.ma.paymentsdk.objects.MA_Billing;
import com.ma.paymentsdk.objects.MA_Constants;
import com.ma.paymentsdk.objects.MA_Dialogs;
import com.ma.paymentsdk.utilities.Logcat;
import com.ma.paymentsdk.utilities.MA_Utility;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

	public static final String tag = "MainActivity";

	private Button butonUpgradeToPremium, buttonUnsub;
	private TextView textStatus, textPhoneNumber, textPincode;
	private final int SDK_CODE = 11;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		initGlobals();
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateUI();
	}

	private void updateUI(){
		if (MA_Billing.getBillingStatus(MainActivity.this)) {
			buttonUnsub.setVisibility(View.VISIBLE);
			butonUpgradeToPremium.setVisibility(View.GONE);
			//Any Activity in your host app can have access to Billing info like phone number and pin code
            textPhoneNumber.setText("Phone number: " + MA_Billing.getCorrectedPhoneNumber(MainActivity.this));
            textPincode.setText("Pin Code: " + MA_Billing.getPinCode(MainActivity.this));
			if (GlobalApplication.isActiveUser) {
				textStatus.setText("you are premium and active user -> show Premium Content/features");
			} else {
				textStatus.setText("Recharge your phone and get access to premium content/features again");
			}
		} else {
			buttonUnsub.setVisibility(View.GONE);
			butonUpgradeToPremium.setVisibility(View.VISIBLE);
			butonUpgradeToPremium.setText("Free user -> Upgrade to premium");
			textStatus.setText("Non subscribed user -> non premium content");
		}
	}

	private void initGlobals() {

		butonUpgradeToPremium = (Button) findViewById(R.id.button_premium);
		textStatus = (TextView) findViewById(R.id.text_status);
		textPhoneNumber = (TextView) findViewById(R.id.text_phoneNumber);
		textPincode = (TextView) findViewById(R.id.text_pincode);
		buttonUnsub = (Button) findViewById(R.id.button_unsubscribe);

	}

	public void onClickFreeFeature(View v){
		Toast.makeText(MainActivity.this,
				"Free feature accessible", Toast.LENGTH_LONG).show();
	}

	public void onClickPremiumFeature(View v){
		if (MA_Billing.getBillingStatus(MainActivity.this)) {
			if (GlobalApplication.isActiveUser) {
				Toast.makeText(MainActivity.this,
						"Premium feature accessible", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(MainActivity.this,
						"Recharge your phone and get access to premium content again", Toast.LENGTH_LONG).show();
			}
		} else {
			showPremiumDialog("This is a premium feature. You need to subscribe to use it");
		}
	}

	private void showPremiumDialog(String message) {
		try {

			AlertDialog.Builder bld = new AlertDialog.Builder(MainActivity.this);
			bld.setMessage(message);
			bld.setNegativeButton(getString(R.string.cancel), null);
			bld.setPositiveButton(getString(R.string.upgrade),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							callPremiumScreen();
						}
					});
			bld.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onClickPremium(View v) {

		callPremiumScreen();

	}

	private void callPremiumScreen(){

		try {
			//sendEvent Function can be called for tracking any event or action you want to track (events should be defined on the interface first)
			//Here we are tracking whenever user presses on upgrade button
			EventHandler.sendEvent(MA_Constants.UPGRADE_EVENT, tag, "", "", new EventTrackerListener() {
				@Override
				public void onSendEventComplete(JSONObject json) {
				}

				@Override
				public void onSendEventError(String error) {
				}
			},MainActivity.this);

			//Call the payment screen
			Intent intent = new Intent(MainActivity.this, MA_BillingActivity.class);
			intent.putExtra(MA_Constants.FROM_WHERE, MA_Constants.InsideApp);
			startActivityForResult(intent, SDK_CODE);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	//This function is to show end user how to unsubscribe from the service
	public void onClickUnsub(View v){

		MA_Dialogs.showUnsubDialogFromAPI(MainActivity.this);

	}

	//This function is to show end user his account details (subscription date, renewal..)
	public void onClickAccountDetails(View v){

		MA_Dialogs.showMyAccountFromAPI(MainActivity.this);

	}

	private String getDetectedPhoneNumber(){

		String phoneNumber = MA_Utility.getPhoneNumber(MainActivity.this);
		if (phoneNumber != null && !phoneNumber.isEmpty()){
			//Phone number detected
		} else {
			//Phone number couldn't be detected
		}
		return phoneNumber;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Logcat.e(tag, "onActivityResult");

		if (requestCode == SDK_CODE) {

			if (resultCode == RESULT_OK) {
				if (MA_Billing.getBillingStatus(MainActivity.this)) {
					//User has successfully subscribed, and you can grant him access to the premium features/content
					GlobalApplication.isActiveUser = true;
					GlobalApplication.isPremium = true;
				} else {
					GlobalApplication.isActiveUser = false;
					GlobalApplication.isPremium = false;
				}
			}
			if (resultCode == RESULT_CANCELED) {
				//Write your code if there's no result
				Logcat.e(tag, "Result: Payment Unsuccessful");
				GlobalApplication.isActiveUser = false;
				GlobalApplication.isPremium = false;
			}
			updateUI();
		}
	}

}
